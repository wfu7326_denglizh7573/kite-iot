package com.donger.iot.core.south.gateway.session;

import java.util.List;

/**
 * 设备回话管理器
 *
 * @author lizhi
 */
public interface DeviceSessionManager {
    /**
     * 创建一个新的session，应该与设备建立连接后创建
     */
    void addSession(AbstractDeviceSession session);

    /**
     * 删除新的session，应该与设备断开连接后删除session
     */
    void removeSession(AbstractDeviceSession session);

    /**
     * 获取所有的Session
     */
    List<AbstractDeviceSession> allSession();
}
