package com.donger.iot.core.center.mq;

import com.donger.iot.core.center.msg.HardWareMsg;

/**
 * 消息队列客户端
 */
public interface MqClient {
    /**
     * 订阅消息
     */
    void sub(String[] topics);

    /**
     * 发布消息
     */
    void pub(HardWareMsg msg);
}
