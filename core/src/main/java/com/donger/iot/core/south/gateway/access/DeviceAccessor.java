package com.donger.iot.core.south.gateway.access;

import com.donger.iot.core.center.device.Device;

/**
 * 设备接入接口
 * 该接口继承Runnable单独一个线程运行，由DeviceAccessorManager进行管理
 * 改接口负责与某个或者某些设备建立连接，可以是主动也可以是被动
 *
 * @author lizhi
 */
public interface DeviceAccessor extends Runnable {
    /**
     * 启动设备接入
     */
    void start();

    /**
     * 停止设备接入
     */
    void stop();

    /**
     * 获取当前启动设备信息
     */

    Device getDevice();

    @Override
    default void run() {
        this.start();
    }
}
