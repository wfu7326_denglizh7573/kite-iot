package com.donger.iot.core.south.gateway.session;

import com.donger.iot.core.center.device.Device;
import lombok.Data;

/**
 * 设备接入的会话管理
 *
 * @author lizhi
 */
@Data
public abstract class AbstractDeviceSession {
    /**
     * 会话对应的设备
     */
    private Device device;

}
