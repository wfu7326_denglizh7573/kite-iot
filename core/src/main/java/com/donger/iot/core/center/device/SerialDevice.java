package com.donger.iot.core.center.device;

import lombok.Data;

/**
 * 串口设备
 *
 * @author lizhi
 */
@Data
public abstract class SerialDevice {

    /**
     * 串口号
     */
    private String port;

    /**
     * 波特率
     */
    private int bps;

    /**
     * 校验位
     */
    private int checkBit;

    /**
     * 数据位
     */
    private int dataBit;

    /**
     * 停止位
     */
    private int stopBit;
}
