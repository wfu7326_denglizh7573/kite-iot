package com.donger.iot.core.center.engine.msg.down.cmd.handler;

import com.donger.iot.core.center.msg.HardWareMsg;

/**
 * 消息处理器
 *
 * @author lizhi
 */
public interface HwMsgDownHandler {
    /**
     * 感兴趣的消息类型
     */
    boolean intrest(HardWareMsg msg);

    /**
     * 消息处理
     *
     * @return 如果消息无需在链中继续处理返回null，返回的object会交由下一个处理器处理
     */
    void handle(HardWareMsg msg);
}
