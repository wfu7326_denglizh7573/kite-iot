package com.donger.iot.core.center.engine.router.dao;

import com.donger.iot.core.center.device.Device;
import com.donger.iot.core.center.engine.router.entity.KiteRouter;

/**
 * 路由表服务接口
 */
public interface RouterService {
    /**
     * 保存路由信息
     * 如果存在刷新，如果不存在添加，一个设备对应一条记录
     *
     * @param router 路由信息
     */
    void save(KiteRouter router);

    /**
     * 查看某个设备路由信息
     *
     * @param device 设备（设备id必填）
     */
    KiteRouter query(Device device);
}
