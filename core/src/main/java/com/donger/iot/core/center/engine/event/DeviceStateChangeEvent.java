package com.donger.iot.core.center.engine.event;

import java.util.Date;

import com.donger.iot.core.center.device.Device;
import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class DeviceStateChangeEvent extends ApplicationEvent {
    private Device device;
    private Date time;
    private DeviceState state;
    private String des;

    public DeviceStateChangeEvent(Object source) {
        super(source);
    }

    public enum DeviceState {
        ONLINE,
        OFFLINE,
        WORKING,
        ERROR;
    }
}
