package com.donger.iot.core.center.engine.router.entity;

import com.donger.iot.core.center.device.Device;
import lombok.Data;

@Data
public class KiteRouter {
    private Device device;
    private KiteMqttGateway gateway;
}
