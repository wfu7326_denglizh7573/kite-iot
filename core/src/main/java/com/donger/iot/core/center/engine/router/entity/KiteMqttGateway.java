package com.donger.iot.core.center.engine.router.entity;

import lombok.Data;

/**
 * 基于MQTT的软网关的配置
 */
@Data
public class KiteMqttGateway {
    /**
     * 该网关订阅的主题
     */
    private String topic;
}
