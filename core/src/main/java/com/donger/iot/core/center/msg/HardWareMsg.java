package com.donger.iot.core.center.msg;

import com.donger.iot.core.center.device.Device;
import lombok.Data;

/**
 * 硬件消息的父类
 * 所有的硬件消息都要直接或间接继承该类
 *
 * @author lizhi
 */
@Data
public abstract class HardWareMsg {
    /**
     * 设备信息（消息来自或者发往那个设备）
     */
    private Device device;

}
