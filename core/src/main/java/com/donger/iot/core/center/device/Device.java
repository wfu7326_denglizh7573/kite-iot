package com.donger.iot.core.center.device;

import lombok.Data;

/**
 * 设备抽象类
 *
 * @author lizhi
 */
@Data
public abstract class Device {
    /**
     * 设备唯一标识
     */
    private String id;
}
