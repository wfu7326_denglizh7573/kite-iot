package com.donger.iot.core.north.app.push;

import com.donger.iot.core.north.app.push.msg.IotAppPushMsg;

/**
 * 消息推送接口
 *
 * @author lizhi
 */
public interface PushService {
    void push(IotAppPushMsg msg);
}
