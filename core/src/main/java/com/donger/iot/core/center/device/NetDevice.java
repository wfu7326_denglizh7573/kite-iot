package com.donger.iot.core.center.device;

import lombok.Data;

/**
 * 网络设备
 *
 * @author lizhi
 */
@Data
public abstract class NetDevice extends Device {
    /**
     * IP地址
     */
    private String ip;
    /**
     * 端口号
     */
    private int port;
}
