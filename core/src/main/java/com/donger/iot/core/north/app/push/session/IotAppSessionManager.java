package com.donger.iot.core.north.app.push.session;

import java.util.List;

public interface IotAppSessionManager<T> {

    /**
     * 创建一个新的session，应该与设备建立连接后创建
     */
    void addSession(T session);

    /**
     * 删除新的session，应该与设备断开连接后删除session
     */
    void removeSession(T session);

    /**
     * 获取所有的Session
     */
    List<T> allSession();
}
