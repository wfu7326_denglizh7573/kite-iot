package com.donger.iot.core.south.gateway.manager;

/**
 * 设备接入管理器
 * 管理DeviceAccessor，针对特定的管理策略进行管理
 *
 * @author lizhi
 */

public interface DeviceAccessorManager {
    /**
     * 是否已经启动
     */
    boolean isRunning();
    /**
     * 是否检查
     */
    boolean isChecking();

    /**
     * 开始
     */
    void start();

    /**
     * 结束
     */
    void stop();

    /**
     * 开始线程检查
     */
    void startCheck();

    /**
     * 结束线程检查
     */
    void stopCheck();

    /**
     * 连接所有管理的设备
     */

    void connect();

    /**
     * 断开所有连接的设备
     */
    void disConnect();

}
