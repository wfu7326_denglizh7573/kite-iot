package com.donger.iot.core.south.gateway.plan;

import com.donger.iot.core.center.device.Device;

import java.util.List;

/**
 * 设备连接计划接口
 * <p>
 * 那些设备需要连接，需要通过连接计划启动相应DeviceAccessor
 */
public interface DeviceAccessPlanervice {

    /**
     * 获取设备连接计划
     *
     * @return 计划为List<Device>
     */

    List<Device> getPlan();
}
