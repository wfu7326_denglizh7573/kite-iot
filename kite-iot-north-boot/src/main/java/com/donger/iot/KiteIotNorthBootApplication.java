package com.donger.iot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KiteIotNorthBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(KiteIotNorthBootApplication.class, args);
    }

}
