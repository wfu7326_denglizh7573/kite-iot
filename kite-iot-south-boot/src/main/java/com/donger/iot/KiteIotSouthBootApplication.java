package com.donger.iot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KiteIotSouthBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(KiteIotSouthBootApplication.class, args);
    }

}
